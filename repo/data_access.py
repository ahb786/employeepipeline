import boto3
from boto3.dynamodb.conditions import Key
import datetime
import json

__session = boto3.Session()
dynamodb = __session.resource('dynamodb')


class DynamoDatabase:
    
    def __init__(self):
        global dynamodb

    def get_item(self, table_name, key):
        try:
            
            table = dynamodb.Table(table_name)
            result = table.get_item(Key=key, ConsistentRead=True)
            if result is not None and "Item" in result.keys():
                return result["Item"]
            else:
                return None
        except Exception as err:
            print(err)
            raise
    