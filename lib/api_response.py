import json
from lib import DecimalEncoder, logger


class CreateApiResponse:
    
    def __init__(self, response=None, api_error_code=None):
        self.__response_message = response
        self.__api_error_code = api_error_code
        self.__logger = logger.Log()
    
    def Bad_Request(self):
        response = {'statusCode': 400, 'body': json.dumps(self.__response_message, default=DecimalEncoder.DecimalEncoder().default, sort_keys=True)}
        print('Response : ', response)
        return response
    
    def Not_Found(self):
        response = {'statusCode': 404, 'body': json.dumps(self.__response_message, default=DecimalEncoder.DecimalEncoder().default, sort_keys=True)}
        print('Response : ', response)
        return response
    
    def Unauthorized(self):
        response = {'statusCode': 401, 'body': json.dumps(self.__response_message, default=DecimalEncoder.DecimalEncoder().default, sort_keys=True)}
        print('Response : ', response)
        return response
    
    def Forbidden(self):
        response = {'statusCode': 403, 'body': json.dumps(self.__response_message, default=DecimalEncoder.DecimalEncoder().default, sort_keys=True)}
        print('Response : ', response)
        return response
    
    def Conflict(self):
        response = {'statusCode': 409, 'body': json.dumps(self.__response_message, default=DecimalEncoder.DecimalEncoder().default, sort_keys=True)}  # Completed with new Error Code
        print('Response : ', response)
        return response
    
    def Internal_Server_Error(self):
        response = {
            'statusCode': 500, 'body': json.dumps({
                "body": "Error Code: 1000. Please try again.", "errorCode": 1000, "errorMessage": "Internal Server Error", "title": "Oops! Something Went Wrong On Our End"
            })
        }
        
        print('Response : ', response)
        return response
    
    def OK(self):
        if isinstance(self.__response_message, str):
            response = {'statusCode': 200, 'body': json.dumps({"message": self.__response_message})}
        else:
            
            if 'modifyDateTime' in self.__response_message: del self.__response_message['modifyDateTime']
            
            response = {'statusCode': 200, 'body': json.dumps(self.__response_message, default=DecimalEncoder.DecimalEncoder().default)}
        print('Response : ', response)
        return response
    
    def Accepted(self):
        return {'statusCode': 202, 'body': json.dumps({"message": 'The request has been accepted for processing.'})}

