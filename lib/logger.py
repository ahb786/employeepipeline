from __future__ import print_function

import os
import traceback
traceback.format_exc()


class Log:
    def info(self,args):
        log_level = os.environ['log_level'] if 'log_level' in os.environ else 'information'
        if log_level.upper() == 'INFORMATION':
            print(args)
  
    def error(self,args):
        print(args, traceback.format_exc())