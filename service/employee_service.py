
from repo import  data_access
from decimal import Decimal
from lib import employee_exception, logger
import boto3, time
from boto3.dynamodb.conditions import Key
import os, json

dynamo_db = boto3.resource('dynamodb')


class EmployeeService:

    def __init__(self):
        self.__data_access = data_access.DynamoDatabase()
        self.__logger = logger.Log()

    def get_detail(self, emp_id = None):
        try:
            emp_detail = None
           
            if emp_id is not None:
                emp_detail = self.__data_access.get_item(table_name= os.environ['employee_table'], key={"id": str(emp_id)})
            else:
                raise employee_exception.EmployeeDetailNotFound('Employee Not Found')
                
            if emp_detail is None:
                raise employee_exception.EmployeeDetailNotFound('Employee Id Found')
                        
            if not emp_detail:
               raise employee_exception.EmployeeDetailNotFound('No such employee')
          
            return emp_detail
        except Exception as err:
            self.__logger.error('Error at get detail {}'.format(err))
            raise
        