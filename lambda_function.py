import json
import boto3
from service import  employee_service
from model import employee
from lib import api_response,employee_exception

__employee_service = employee_service.EmployeeService()

def lambda_handler(event, context):
    try:
        if not event['pathParameters'].get('empId'):
            return api_response.CreateApiResponse("Emp Id not Found",404).Not_Found()
        emp_detail = __employee_service.get_detail(emp_id=event.get('empId'))
        if emp_detail is None:
            return api_response.CreateApiResponse("Emp detail not found",404).Bad_Request()
        
        print(emp_detail)
        return api_response.CreateApiResponse(response=employee.Employee(employee=emp_detail, is_none = False).json()).OK()
    
    #resp = table.get_item(Key={"id": emp_id })
    #print(resp['Item'])
    #response = {
    #    "statusCode": 200,
    #   "body": json.dumps(resp['Item'])
    #}
    #print(response)
    #return response
	
    except employee_exception.EmployeeDetailNotFound as e:
        print('Error at Get employee detail'.format(e.args))
        return api_response.CreateApiResponse("Emp Id not Found",400).Not_Found()
        
    except Exception as err:
        print('Error at Get employee detail'.format(err.args))
        return api_response.CreateApiResponse().Internal_Server_Error()
    
    