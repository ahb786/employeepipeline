from lib import logger
#Detail information about employee.
class Employee():
    def __init__(self,employee, json = None, is_none = False, is_external= False):
        self.emp_detail = employee
        self.json_emp = json
        self.is_none = is_none
        self.isExternal = is_external # for external webhook api
        self.id = None
        self.name = None
      
        
    def json(self):
        json_emp = {}
        __logger = logger.Log()
        try:
            if self.emp_detail is not None and self.is_none == False:
                print("self emp detail is:"+str(self.emp_detail))
                if self.emp_detail['id'] is not None: json_emp['id'] = self.emp_detail['id']
                if self.emp_detail['name'] is not None: json_emp['name'] = self.emp_detail['name']
                
                return json_emp
            
            elif self.emp_detail is not None and self.is_none == True:
                json_emp['id'] = self.emp_detail['id'] if self.emp_detail['id'] is not None else ""
                json_emp['name'] = self.emp_detail['name'] if self.emp_detail['name'] is not None else ""
     
                return json_emp
            else:
                return None
        except Exception as error:
            __logger.error('Error at Employee Json parser {}'.format(error))
            raise Exception('Error while employee parser')
            
    def employee(self):
        __logger = logger.Log()
        try:
            if self.json_emp is not None:
                emp_obj = Employee()
          
                    
                emp_obj.id = self.json_emp['id'] if 'id' in self.json_emp and self.json_emp['id'] is not None else None
                emp_obj.name = self.json_emp['name'] if 'json_emp' in self.json_emp and self.json_emp['name'] is not None else None
                
                return emp_obj
            else:
                return None
    
        except Exception as error:
            __logger.error('Error during employee parse {}'.format(error))
            raise Exception('Error at employee parse')