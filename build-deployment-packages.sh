#!/bin/bash
echo "welcome"

BUILD_DIR=$PWD/build
echo  Build directory : $BUILD_DIR

if [ -d $BUILD_DIR ]; then
  echo removing Build directory : $BUILD_DIR
  rm -rf $BUILD_DIR
fi

mkdir -p $BUILD_DIR

zip -r $BUILD_DIR/employee.zip *

if [ $? -gt 0 ]; then
  echo "Build packaging failed!!!"
  exit $?
fi

exit 0
